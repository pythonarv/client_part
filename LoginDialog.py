import wx
from pubsub import pub
import io
from PIL import Image
import threading

class LoginDialog(wx.Dialog):
    """
    Class to define login dialog
    """

    def __init__(self, parent, login_screen_data=None):
        """Constructor"""
        wx.Dialog.__init__(self, None, title="Login")
        if login_screen_data:
            login_screen_data = {'host': '', 'username': '', 'password': '', **login_screen_data}
        else:
            login_screen_data = {'host': '', 'username': '', 'password': ''}
        self.parent = parent
        self.logged_in = False
        self.connected = False
        self.init_data = None

        sizer = wx.GridBagSizer(10, 10)

        addr_lbl = wx.StaticText(self, label="Address:")
        self.addr = wx.TextCtrl(self, value=login_screen_data['host'])

        sizer.Add(addr_lbl, (0, 0), (1, 1), wx.ALIGN_RIGHT | wx.LEFT | wx.RIGHT | wx.TOP, 10)
        sizer.Add(self.addr, (0, 1), (1, 1),  wx.ALIGN_LEFT | wx.EXPAND | wx.LEFT | wx.RIGHT | wx.TOP, 10)

        user_lbl = wx.StaticText(self, label="Username:")
        self.user = wx.TextCtrl(self, value=login_screen_data['username'])
        sizer.Add(user_lbl, (1, 0), (1, 1), wx.ALIGN_RIGHT | wx.LEFT | wx.RIGHT, 10)
        sizer.Add(self.user, (1, 1), (1, 1), wx.EXPAND | wx.ALIGN_LEFT | wx.LEFT | wx.RIGHT, 10)

        p_lbl = wx.StaticText(self, label="Password:")
        # p_sizer.Add(p_lbl, 0, wx.ALL | wx.CENTER, 5)
        self.password = wx.TextCtrl(self, style=wx.TE_PASSWORD | wx.TE_PROCESS_ENTER, value=login_screen_data['password'])
        self.password.Bind(wx.EVT_TEXT_ENTER, self.onLogin)
        # p_sizer.Add(self.password, 0, wx.ALL, 5)
        sizer.Add(p_lbl, (2, 0), (1, 1), wx.ALIGN_RIGHT | wx.LEFT | wx.RIGHT, 10)
        sizer.Add(self.password, (2, 1), (1, 1), wx.EXPAND | wx.ALIGN_LEFT | wx.LEFT | wx.RIGHT, 10)

        btn = wx.Button(self, label="Login")
        btn.Bind(wx.EVT_BUTTON, self.onLogin)
        sizer.Add(btn, (3, 0), (1, 2), wx.EXPAND | wx.ALIGN_CENTER | wx.LEFT | wx.RIGHT, 20)

        self.message = wx.StaticText(self, style=wx.ALIGN_CENTER)
        self.message.SetForegroundColour(wx.RED)
        sizer.Add(self.message, (4, 0), (1, 2), wx.EXPAND | wx.ALIGN_CENTER | wx.LEFT | wx.RIGHT, 20)

        sizer.AddGrowableCol(0)
        sizer.AddGrowableCol(1)
        self.SetSizer(sizer)

        pub.subscribe(self.onDisconnect, 'disconnect')
        pub.subscribe(self.auth_success, 'auth_success')
        pub.subscribe(self.auth_fail, 'auth_fail')

        self.Bind(wx.EVT_CLOSE, self.onClose)

    def onClose(self, e):
        pub.unsubscribe(self.onDisconnect, 'disconnect')
        pub.unsubscribe(self.auth_success, 'auth_success')
        pub.unsubscribe(self.auth_fail, 'auth_fail')
        e.Skip()


    def onLogin(self, event):
        if self.validate():
            if not self.connected:
                host = self.addr.GetValue()
                host = host.split(':')
                self.connected = self.parent.openConnection(host[0], int(host[1]))
                if not self.connected:
                    self.setMessage("Ошибка подключения")
                    return
            self.auth()
        else:
            self.setMessage("Заполните все поля")

    def onDisconnect(self):
        self.connected = False
        self.setMessage("Превышено количество попыток авторизации. Соединение разорвано")

    def auth(self):
        username = self.user.GetValue()
        password = self.password.GetValue()
        creds = {'username': username, 'password': password}
        pub.sendMessage('auth', creds=creds)

    def auth_success(self, data):
        self.init_data = data['init_data']
        # image = Image.open(io.BytesIO(data['init_data']['image']))
        # image.save('test.png','PNG')
        self.EndModal(wx.ID_OK)

    def auth_fail(self, data):
        self.setMessage('Неверный логин или пароль')

    def setConnected(self, connected):
        self.connected = connected
        self.addr.Enable(not connected)

    def setMessage(self, msg):
        self.message.SetLabelText(msg)

    def validate(self):
        return not self.addr.IsEmpty() and \
               not self.user.IsEmpty() and \
               not self.password.IsEmpty()

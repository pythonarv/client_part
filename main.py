import socket
from sys import exit
import wx
from SocketClient import SocketClient
from LoginDialog import LoginDialog
from pubsub import pub
from MainPanel import MainPanel
import os.path
import json


class MainFrame(wx.Frame):
    """"""

    def __init__(self):
        """Constructor"""
        wx.Frame.__init__(self, None, title="ImageTagger")
        self.config_path = 'config.json'
        self.config = self.loadConfig()
        self.socket = None
        self.Bind(wx.EVT_CLOSE, self.onExit)
        self.exited = False
        # Ask user to login

        init_data = self.showLoginForm()
        self.panel = MainPanel(self, init_data, self.config)
        self.sizer = wx.BoxSizer(wx.VERTICAL)
        self.sizer.Add(self.panel, 1, wx.EXPAND)
        self.SetSizer(self.sizer)

        self.SetMinSize((700, 700))
        self.Show()
        self.Maximize(True)
        self.Layout()

    def showLoginForm(self):

        login_screen_data = None
        if 'login_screen' in self.config:
            login_screen_data = self.config['login_screen']
        login_dialog = LoginDialog(self, login_screen_data)
        code = login_dialog.ShowModal()
        login_dialog.Close()
        login_dialog.Destroy()

        if code != wx.ID_OK:
            exit()

        login_screen_data = {}
        login_screen_data['username'] = login_dialog.user.GetValue()
        login_screen_data['password'] = login_dialog.password.GetValue()
        login_screen_data['host'] = login_dialog.addr.GetValue()
        self.config['login_screen'] = login_screen_data
        self.saveConfig()
        return login_dialog.init_data

    def emit(self, event, data=None):
        self.socket.emit(event, data)

    def openConnection(self, host, port):
        if self.socket:
            del self.socket
            self.socket = None
        try:
            self.socket = SocketClient(host, port)
        except socket.error:
            return False
        self.socket.start()
        pub.subscribe(self.emit, 'socket_emit')
        pub.subscribe(self.onDisconect, 'disconnect')
        return True

    def closeAndShowForm(self):
        self.Hide()
        self.exited = True
        init_data = self.showLoginForm()
        self.panel.Destroy()

        self.panel = MainPanel(self, init_data, self.config)
        self.sizer.Add(self.panel, 1, wx.EXPAND)
        self.Layout()
        self.exited = False
        self.Show()

    def onDisconect(self):
        if not self.exited:
            self.closeAndShowForm()

    def onExit(self, event):
        self.exited = True
        pub.sendMessage('disconnect')
        self.closeAndShowForm()

    def loadConfig(self):
        config = self.defaultConfig()
        if os.path.isfile(self.config_path):
            with open(self.config_path, 'r', encoding='utf8') as f:
                config = {**config, **json.load(f)}
        else:
            self.saveConfig(config)
        return config

    def saveConfig(self, config = None):

        config = config if config else self.config

        with open(self.config_path, 'w+', encoding='utf8') as f:
            json.dump(config, f)

    @staticmethod
    def defaultConfig():
        return {
            'classes_hotkeys': ['Z', 'X', 'C', 'V', 'B', 'N', 'M'],
            'back_hotkey': 'BACKSPACE',
            'skip_hotkey': 'SPACE',
            'font_size': 20
        }


if __name__ == "__main__":
    app = wx.App(False)
    frame = MainFrame()
    app.MainLoop()

import wx
from pubsub import pub
import io
import os
import json

class MainPanel(wx.Panel):
    """"""

    def __init__(self, parent, init_data, config):
        """Constructor"""
        wx.Panel.__init__(self, parent)

        self.config = config

        f = self.GetFont()

        f.SetPointSize(self.config['font_size'])

        self.SetFont(f)

        # self.hotkeys = ['Z', 'X', 'C', 'V', 'B', 'N', 'M']

        self.hotkeys = self.config['classes_hotkeys']
        self.back_hotkey = getattr(wx, 'WXK_'+self.config['back_hotkey'].upper())
        self.skip_hotkey = getattr(wx, 'WXK_'+self.config['skip_hotkey'].upper())

        self.classes = init_data['classes']

        self.class_btns = []

        self.skiped = False

        self.imageExists = True

        sizer = wx.BoxSizer(wx.VERTICAL)
        self.button_sizer = wx.WrapSizer()

        id = 0
        for clas in self.classes:
            id +=1
            btn = wx.Button(self, label=clas)
            self.class_btns.append(btn)

            btn.Bind(wx.EVT_BUTTON, self._onClassSelect)

            self.button_sizer.Add(btn, 10, wx.ALL, 5)

        btn = wx.Button(self, label="Пропустить")
        self.class_btns.append(btn)
        self.button_sizer.Add(btn, 10, wx.ALL | wx.EXPAND, 5)
        btn.Bind(wx.EVT_BUTTON, self.skipImage)

        self.header_sizer = wx.BoxSizer(wx.HORIZONTAL)

        togler = wx.ToggleButton(self, label="Пропущенные изображения")
        prev_button = wx.Button(self, label="Назад")

        togler.Bind(wx.EVT_TOGGLEBUTTON, self._skipImageToggle)
        prev_button.Bind(wx.EVT_BUTTON, self.prevImage)

        self.header_sizer.Add(togler, 0, wx.ALL | wx.ALIGN_LEFT, 5)
        self.header_sizer.Add(prev_button, 0, wx.ALL | wx.ALIGN_RIGHT, 5)

        sizer.Add(self.header_sizer, 0, wx.EXPAND | wx.ALIGN_CENTER)

        self.originalImage = None
        self.control = wx.StaticBitmap(self, -1)
        self.nextImage(init_data['image'])
        sizer.Add(self.control, 0, wx.ALL | wx.ALIGN_CENTER, 5)
        sizer.Add(self.button_sizer, 0, wx.ALIGN_CENTER)

        self.Bind(wx.EVT_SIZE, self._onSize)
        self.Bind(wx.EVT_CHAR_HOOK, self._onKeyPressed)

        self.SetSizer(sizer)

        pub.subscribe(self.nextImage, 'socket_image')

    def _skipImageToggle(self, e):
        self.skiped = e.IsChecked()
        self.requestNextImage()

        # if not self.imageExists:
        #     pass

    def prevImage(self, e):
        pub.sendMessage('socket_emit', event='prevImage')

    def skipImage(self, e=None):
        self.tagImage('')
        self.requestNextImage()

    def nextImage(self, data):
        if data:
            self.enableButton(True)
            self.imageExists = True
            self.originalImage = wx.Image(io.BytesIO(data))
            img_size = self.originalImage.GetSize()
            self.imageProportion = img_size[0]/img_size[1]
            self.bitmap = wx.Bitmap(self._fitImage(self.GetSize()))
            self.control.SetBitmap(self.bitmap)
            self.Refresh()
            self.imageExists = True
            self.control.Show()
            self.Layout()
        else:
            wx.MessageBox('Картинки закончились')
            # self.enableButton(False)
            self.imageExists = False
            self.control.Hide()

        self.Enable(True)
        self.SetFocus()

    def enableButton(self, enabled):
        for btn in self.class_btns:
            btn.Enable(enabled)

    def tagImage(self, clas):
            pub.sendMessage('socket_emit', event='tagImage', data=clas)

    def requestNextImage(self):
        self.Enable(False)
        pub.sendMessage('socket_emit', event='nextImage', data=self.skiped)

    def _onClassSelect(self, e):
        self.appendLabel(e.GetEventObject().GetLabel())
        # self.tagImage(e.GetEventObject().GetLabel())
        # self.requestNextImage()

    def appendLabel(self, label):
        if self.imageExists:
            self.tagImage(label)
            self.requestNextImage()

    def _onKeyPressed(self, e):
        keycode = e.GetKeyCode()
        if keycode != wx.WXK_NONE:
            if chr(keycode) in self.hotkeys:
                index = self.hotkeys.index(chr(keycode))
                if index < len(self.classes):
                    # self.tagImage(self.classes[index])
                    self.class_btns[index].SetFocus()
                    self.appendLabel(self.classes[index])
                    # self.requestNextImage()
            elif keycode == self.skip_hotkey:
                # self.tagImage('')
                self.class_btns[-1].SetFocus()
                self.appendLabel('')
                # self.requestNextImage()
            elif keycode == self.back_hotkey:
                self.prevImage(None)

    def _fitImage(self, max_size):
        W, H = max_size
        NewH = H / 100 * 80

        buttons_size = self.button_sizer.CalcMin()
        header_size = self.header_sizer.CalcMin()

        btns_H = buttons_size[1] + header_size[1]

        if H > btns_H and  NewH > (H - btns_H):
            NewH = (H - btns_H)

        NewW = NewH * self.imageProportion

        if NewW > W:
            NewW = W
            NewH = NewW / self.imageProportion

        return self.originalImage.Scale(NewW, NewH)

    def _onSize(self, e):

        if self.originalImage:
            self.control.SetBitmap(wx.Bitmap(self._fitImage(e.GetSize())))
            self.Refresh()
        e.Skip()